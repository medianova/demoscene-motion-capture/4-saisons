# Présentation

Le projet DemoScene a pour but de présenter un dossier pour les fêtes de la Lumière 2022. Dans ce dossier, nous voulons montrer notre capacité à créer et à innover une œuvre artistique unique et interactive avec le public à partir d code informatique.   

# Contexte

L'art numérique désigne tout art réalisé avec des dispositifs numériques, que ce soit avec des logiciels dédiés ou bien par les moyens de visualisation comme une projection. Ceci permet ainsi de créer un œuvre à la fois interactive avec le public, générative par les interactions de ce dernier et immersive dans l'atmosphère et les émotions produites. L'interaction des œuvres que nous voulons créer rend chacune d'elle unique et éphémère. En effet, chaque intéraction génère des éléments aléatoires dans la scène dépendent également des déplacements des foules.   

# Thème

Nous avons choisis "La Vie" comme thème général pour ce projets. L'intéraction principale étant le déplacement des foules, ce thème est assez explicite et nous laisse une palette de possibilités pour nous exprimer à travers le code.

Nous avons divisé le projet en 3 sous-projets créatifs avec des thèmes que nous avons définis et qui sont rattachés au thème général.


## Les sous-projets

Le projet DemoScene regroupe en réalité trois œuvres à thème différentes basées sur la capture des mouvements de personnes :   

 - Oshi - Les fractales :

    Oshi (étoile en japonais), nom de la première étape de la vie, est une référence à l’espace et par extension aux prémices de la vie. Le but de ce sous-projet est, de manière conceptuelle, de représenter l’espace à ses premiers instants. Pour cela, nous basons notre travail sur les fractales et les teintes liées à l’espace, car ce tout est pour nous le plus parlant pour le public, et donc le meilleur moyen de le faire comprendre.
    Concernant les fractales, ce choix n’est pas anodin, car nous en retrouvons dans l’infiniment grand et l’infiniment petit, et ont toujours constitué notre vie. Par exemple, nous en retrouvons dans les vaisseaux sanguins, le sel, les nervures des feuilles, l’univers en lui-même…  

- Vivaldi - Les 4 saisons :

    Vivaldi, en référence au violoniste italien du même nom et auteur de l'oeuvre musicale les 4 saisons. Derrière ce thème se cache la volonté de créer une oeuvre jouant en grande partie sur les couleurs et les formes que peuvent évoquer chacune des saisons. Ici, l'oeuvre final est abstraite et de ce fait, chacun des spectateurs peut ainsi y voir ce qu'il imagine et se créer son propre univers autour d'une même œuvre.

- Hajimari to owari - Le jeu de la vie :

    Hajimari to owari (Du début à la fin en japonais) est le nom de ce 3ème projet. Le jeu de la vie est un programme informatique simulant la création de la vie par des cellules autonomes. Il fut écrit par Conway, un mathématicien du 20e siecle. Nous nous basons sur son travail pour réaliser une visualisation d'art génératif, où le lieu de naissance des cellules est selon la position d'un visage. Nous voulons exprimer à travers cela que la vie se créée par nos présences, et que quand nous partons, ce que nous avons laissé s'éteind progressivement.

# L'équipe

L'équipe est composée majoritairement de développeurs ayant une vision et des approches créatives variées :

| Membre            | Rôle                 | Etude           |

| ----------------- | -------------------- | --------------- |

| David HERVE       | Chef de projet       | M1 Logiciel     |

| César WATRIN      | Développeur          | M1 Web          |

| Emeric LAMBERT    | Développpeur         | M1 Web          |

| Nathanael ALLARD  | Développeur          | M1 Web          |

| Gary JACQUELIN    | Développeur          | M1 Logiciel     |

| Julien SIBUE      | Développeur          | M1 Logiciel     |

| Baptiste MATRAY   | Directeur Artistique | B1 Créatif      |

| Mathiste POINTARD | Développeur          | B1 Informatique |

| Damien LORENTE    | Développeur          | M1 Web          |

# Avancement

Pour l'instant, nous sommes en phase de prototypage et les technologies comme la Micrososft Kinect ne sont pas encore maîtrisées dans l'intégralité. L'objectif de réaliser une œuvre fonctionnelle selon notre direction artistique est notre priorité. Le choix de mettre en place plusieurs installations au lieu d'une seule, nécessite un travail plus conséquent. Cela permet aussi de développer en petite équipe plus rapidement et avec une réelle implication de chacun. Pour faciliter le développement de chaque projet, une api avec des données de test et le code d'utilisation de la Kinect ont été mis en commun. Chacun est alors libre d'enrichir la documentation pour les autres.   

# Et ensuite ?

La prochaine étape du projet est l'installation et le test dans un environnement réel de nos réalisations. C'est une étape important sur le rendu et les paramétrages à faire. Conscient du travail, à réaliser, nous essayons de mettre et de maintenir un environnement de développement propice aux innovations et aux réalisations. Nous nous basons alors sur les méthodologies agiles pour la gestion du projet.   

Nous gardons en vue comme objectif final, de présenter nos réalisations devant un vrai public. Le retour du public est une chose précieuse qui nous permettra de nous ajuster pour notre dossier pour les fêtes de la Lumière 2022. Pour que cela soit possible, nous devons rester flexibles sur nos choix technologiques (comme la Kinect) et concrétiser le travail produit afin d'assurer une certaine fiabilité de notre code et de sa valorisation.
