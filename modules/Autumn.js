class Autumn {
    noiseIntensity = 13
    noiseDetail = 1
    mult = DEFAULT_MULT
    deg = DEFAULT_DEG
    bg = DEFAULT_BG

    constructor(vector) {
        points.push(vector)
        //cleanup(this.bg, this.noiseDetail, this.deg)
    }

    display(){
        for (let i = 0; i < points.length; i++) {

            let r = map(points[i].x, 0, width, 241, 242)
            let g = map(points[i].y, 0, height, 101, 203)
            let b = map(points[i].x, 0, width, 6, 19)

            fill(r, g, b)

            let angle = map(noise(points[i].x * this.mult, points[i].y * this.mult), 0, 1, 0, this.noiseIntensity)

            points[i].add( createVector(cos(angle), sin(angle)) )

            ellipse(points[i].x, points[i].y, 0.2)
        }

        if (points.length > 300) {
            points.shift()
        }
    }
}