class Spring {
    noiseIntensity = 10
    noiseDetail = DEFAULT_NOISE
    mult = DEFAULT_MULT
    deg = DEFAULT_DEG
    bg = DEFAULT_BG

    constructor(vector) {
        points.push(vector)
        //cleanup(this.bg, this.noiseDetail, this.deg)
        this.noiseIntensity = this.randomIntFromInterval(10, 100)
    }

    display() {
        for (let i = 0; i < points.length; i++) {

            let r = map(points[i].x, 0, width, 222, 244)
            let g = map(points[i].y, 0, height, 232, 140)
            let b = map(points[i].x, 0, width, 142, 236)

            fill(r, g, b);

            let angle = map(noise(points[i].x * this.mult, points[i].y * this.mult), 0, 1, 0, this.noiseIntensity)

            points[i].add( createVector(cos(angle), sin(angle)) )

            ellipse(points[i].x, points[i].y, 1)
        }

        if (points.length > 100) {
            points.shift()
        }
    }

    randomIntFromInterval(min, max) { // min and max included
        return Math.floor(Math.random() * (max - min + 1) + min)
    }
}