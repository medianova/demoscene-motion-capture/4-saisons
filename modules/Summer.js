class Summer {
    noiseIntensity = 200
    noiseDetail = DEFAULT_NOISE
    mult = DEFAULT_MULT
    deg = DEFAULT_DEG
    bg = DEFAULT_BG

    constructor(vector) {
        points.push(vector)
        //cleanup(this.bg, this.noiseDetail, this.deg)
    }

    display(){
        for (let i = 0; i < points.length; i++) {

            this.mult = this.randomIntFromInterval(0.00015, 0.000015)

            let r = map(points[i].x, 0, width, 194, 119)
            let g = map(points[i].y, 0, height, 160, 210)
            let b = map(points[i].x, 0, width, 226, 4)

            fill(r, g, b)

            let angle = map(noise(this.randomIntFromInterval(0, 100) * this.mult, this.randomIntFromInterval(0, 100) * this.mult), 0, 1, 0, this.noiseIntensity)

            points[i].add( createVector(0, sin(angle)/this.randomIntFromInterval(-10, 10)) )

            ellipse(points[i].x, points[i].y, 1)
        }

        if (points.length > 100) {
            points.shift()
        }
    }

    randomIntFromInterval(min, max) { // min and max included
        return Math.random() < 0.5 ? ((1-Math.random()) * (max-min) + min) : (Math.random() * (max-min) + min)
    }
}