class Winter {
    noiseIntensity = 800
    noiseDetail = DEFAULT_NOISE
    mult = DEFAULT_MULT
    deg = DEFAULT_DEG
    bg = DEFAULT_BG

    constructor(vector) {
        points.push(vector)
        //cleanup(this.bg, this.noiseDetail, this.deg)
    }

    display(){
        for (let i = 0; i < points.length; i++) {

            let r = map(points[i].x, 0, width, 247, 116)
            let g = map(points[i].y, 0, height, 191, 247)
            let b = map(points[i].x, 0, width, 249, 212)

            fill(r, g, b)

            let angle = map(noise(points[i].x * this.mult, points[i].y * this.mult), 0, 1, 0, this.noiseIntensity)

            points[i].add( createVector(this.randomIntFromInterval(-10, 10), sin(angle)+ this.randomIntFromInterval(-10, 10)) )

            ellipse(points[i].x, points[i].y, 0.2)
        }

        if (points.length > 60) {
            points.shift()
        }
    }

    randomIntFromInterval(min, max) { // min and max included
        return Math.floor(Math.random() * (max - min + 1) + min)
    }
}