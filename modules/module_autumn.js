let points = [];
let mult = 0.015;
let noiseIntensity = 13; //entre 50 et 700
let ellipses = [];

function setup() {
    createCanvas(windowWidth, windowHeight);
    background(30);
    angleMode(360);
    noiseDetail(1);

    window.socket.emit(window.fakerType);
    window.socket.on(window.fakerType, (data) => {
        for (var i = 0; i < data.nb_peoples; i++) {
            lx = data.peoples[i].left_hand.x * windowWidth;
            ly = data.peoples[i].left_hand.y * windowHeight;
            rx = data.peoples[i].right_hand.x * windowWidth;
            ry = data.peoples[i].right_hand.y * windowHeight;
            draw(lx, ly);
            draw(rx, ry);
        }
    });
}

function draw(x, y) {
    noStroke();

    if (x && y) {
        let p = createVector(x, y);
        points.push(p);
    } else if (mouseIsPressed) {
        let p = createVector(mouseX, mouseY);
        points.push(p);
    }

    for (let i = 0; i < points.length; i++) {

        let r = map(points[i].x, 0, width, 241, 242);
        let g = map(points[i].y, 0, height, 101, 203);
        let b = map(points[i].x, 0, width, 6, 19);

        fill(r, g, b);

        let angle = map(noise(points[i].x*mult, points[i].y*mult), 0, 1, 0, noiseIntensity);

        points[i].add(createVector(cos(angle), sin(angle)));

        ellipse(points[i].x, points[i].y, 0.2);
    }

    if (points.length > 300) {
        points.shift();
    }
}
