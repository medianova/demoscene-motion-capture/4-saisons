let points = [];
let mult = 0.015;
let noiseIntensity = 10; //entre 50 et 700
let ellipses = [];

function setup() {
    createCanvas(windowWidth, windowHeight);
    background(30);
    angleMode(360);
    noiseDetail(500);

    window.socket.emit(window.fakerType);
    window.socket.on(window.fakerType, (data) => {
        for (var i = 0; i < data.nb_peoples; i++) {
            lx = data.peoples[i].left_hand.x * windowWidth;
            ly = data.peoples[i].left_hand.y * windowHeight;
            rx = data.peoples[i].right_hand.x * windowWidth;
            ry = data.peoples[i].right_hand.y * windowHeight;
            draw(lx, ly);
            draw(rx, ry);
        }
    });
}

function draw(x, y) {

    noStroke();

    if (x && y) {
        let p = createVector(x, y);
        points.push(p);
        noiseIntensity = randomIntFromInterval(10, 100);
    } else if (mouseIsPressed) {
        let p = createVector(mouseX, mouseY);
        points.push(p);
        noiseIntensity = randomIntFromInterval(10, 100);
    }

    for (let i = 0; i < points.length; i++) {

        let r = map(points[i].x, 0, width, 222, 244);
        let g = map(points[i].y, 0, height, 232, 140);
        let b = map(points[i].x, 0, width, 142, 236);

        fill(r, g, b);

        let angle = map(noise(points[i].x*mult, points[i].y*mult), 0, 1, 0, noiseIntensity);

        points[i].add(createVector(cos(angle), sin(angle)));

        ellipse(points[i].x, points[i].y, 0.2);
    }

    if (points.length > 100) {
        points.shift();
    }
}

function randomIntFromInterval(min, max) { // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min)
}
