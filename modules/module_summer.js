let points = [];
let mult = 0.015;
let noiseIntensity = 200; //entre 50 et 700
let ellipses = [];

function setup() {
    createCanvas(windowWidth, windowHeight);
    background(30);
    noiseDetail(500);
    window.socket.emit(window.fakerType);
    window.socket.on(window.fakerType, (data) => {
        for (var i = 0; i < data.nb_peoples; i++) {
            lx = data.peoples[i].left_hand.x * windowWidth;
            ly = data.peoples[i].left_hand.y * windowHeight;
            rx = data.peoples[i].right_hand.x * windowWidth;
            ry = data.peoples[i].right_hand.y * windowHeight;
            draw(lx, ly);
            draw(rx, ry);
        }
    });
}

function draw(x, y) {
    noStroke();

    if (x && y) {
        let p = createVector(x, y);
        points.push(p);
    } else if (mouseIsPressed) {
        let p = createVector(mouseX, mouseY);
        points.push(p);
    }

    for (let i = 0; i < points.length; i++) {

        mult = randomIntFromInterval(0.00015, 0.000015);

        let r = map(points[i].x, 0, width, 194, 119);
        let g = map(points[i].y, 0, height, 160, 210);
        let b = map(points[i].x, 0, width, 226, 4);

        fill(r, g, b);

        let angle = map(noise(randomIntFromInterval(0, 100)*mult, randomIntFromInterval(0, 100)*mult), 0, 1, 0, noiseIntensity);

        points[i].add(createVector(0, sin(angle)/randomIntFromInterval(-10, 10)));

        ellipse(points[i].x, points[i].y, 1);
    }

    if (points.length > 100) {
        points.shift();
    }
}

function randomIntFromInterval(min, max) { // min and max included
    return Math.random() < 0.5 ? ((1-Math.random()) * (max-min) + min) : (Math.random() * (max-min) + min);
}
