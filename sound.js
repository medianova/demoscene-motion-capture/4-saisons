let sound = new Howl({
    src: ['/sounds/4saisons.mp3', '/sounds/violonxbasse.mp3'],
    autoplay: true,
    loop: true
})

$(document).ready(function(){
    console.log('ready', sound)
    let id1 = sound.play()
    sound.fade(0, 1, 4000, id1)
})

let random = new Howl({
    src: [
        "/sounds/random/r_effet_violon.mp3",
        "/sounds/random/r_guitare_ete.mp3",
        "/sounds/random/r_harpe_hiver.mp3",
        "/sounds/random/r_piano_automne.mp3",
    ],
    autoplay: false,
    loop: true
})

$("body").on("click", function(){
    console.log("random")
    let id2 = random.play()
    random.fade(0, 1, 4000, id2)
});